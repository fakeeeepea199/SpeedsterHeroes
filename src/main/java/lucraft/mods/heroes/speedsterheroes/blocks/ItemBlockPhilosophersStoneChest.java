package lucraft.mods.heroes.speedsterheroes.blocks;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;

public class ItemBlockPhilosophersStoneChest extends ItemBlock {

	public ItemBlockPhilosophersStoneChest(Block block) {
		super(block);
		this.setMaxStackSize(1);
	}

}
